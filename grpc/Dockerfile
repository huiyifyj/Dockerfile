# compile build stage
FROM rust:slim AS builder

ENV PROTOBUF_VERSION 3.18.0

ENV PATH /usr/local/protobuf/bin:$PATH

RUN set -eux; \
	buildDeps="ca-certificates curl unzip"; \
	apt-get update && apt-get install -y --no-install-recommends $buildDeps; \
	mkdir /usr/local/protobuf && cd /usr/local/protobuf; \
	curl -sSL "https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VERSION}/protoc-${PROTOBUF_VERSION}-linux-x86_64.zip" -o protoc.zip; \
	unzip protoc.zip && rm -rf protoc.zip; \
	# smoke test protoc
	protoc --version; \
	# compile gRPC for rust tools
	cargo --version; \
	cargo install protobuf-codegen; \
	cargo install grpcio-compiler; \
	# copy necessary binaries to specify folder
	cp `which protoc-gen-rust` /usr/local/protobuf/bin/; \
	cp `which grpc_rust_plugin` /usr/local/protobuf/bin/

# main stage
FROM debian:bullseye-slim

ENV PATH /usr/local/protobuf/bin:$PATH

COPY --from=builder /usr/local/protobuf/ /usr/local/protobuf/

WORKDIR /data

CMD ["protoc"]
