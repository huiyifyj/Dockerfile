## gRPC

> [gRPC](https://github.com/tikv/grpc-rs) docker image for rust language.

![pulls](https://img.shields.io/docker/pulls/huiyifyj/grpc?logo=docker&style=flat-square&label=pulls)

#### Install

```shell
docker pull huiyifyj/grpc
```

#### Usage

```shell
docker run -v "$PWD:/data" huiyifyj/grpc bash -c \
    "protoc --rust_out=. --grpc_out=. \
    --plugin=protoc-gen-grpc=\`which grpc_rust_plugin\` \
    test.proto"
```
