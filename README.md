# Dockerfile

> Some docker images for myself...

| Image | Layers | Tags |
| - | - | - |
| [Ghost](https://github.com/TryGhost/Ghost) | [![ghost](https://img.shields.io/docker/pulls/huiyifyj/ghost?logo=docker&style=flat-square&label=pulls)](https://hub.docker.com/r/huiyifyj/ghost) | [`latest`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/ghost/Dockerfile) |
| [gRPC](https://github.com/tikv/grpc-rs) | [![grpc](https://img.shields.io/docker/pulls/huiyifyj/grpc?logo=docker&style=flat-square&label=pulls)](https://hub.docker.com/r/huiyifyj/grpc) | [`latest`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/grpc/Dockerfile) |
| [pwsh](https://github.com/PowerShell/PowerShell) | [![pwsh](https://img.shields.io/docker/pulls/huiyifyj/pwsh?logo=docker&style=flat-square&label=pulls)](https://hub.docker.com/r/huiyifyj/pwsh) | [`latest`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/pwsh/Dockerfile) |
| [redis](https://github.com/redis/redis) | [![redis](https://img.shields.io/docker/pulls/huiyifyj/redis?logo=docker&style=flat-square&label=pulls)](https://hub.docker.com/r/huiyifyj/redis) | [`latest`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/redis/Dockerfile) |
| scratch-go | [![scratch-go](https://img.shields.io/docker/pulls/huiyifyj/scratch-go?logo=docker&style=flat-square&label=pulls)](https://hub.docker.com/r/huiyifyj/scratch-go) | [`latest`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/scratch-go/Dockerfile) |
| [thrift](https://github.com/apache/thrift) | [![thrift](https://img.shields.io/docker/pulls/huiyifyj/thrift?logo=docker&style=flat-square&label=pulls)](https://hub.docker.com/r/huiyifyj/thrift) | [`0.13.0 (latest)`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/thrift/0.13.0/Dockerfile) <br> [`0.10.0`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/thrift/0.10.0/Dockerfile) |
