## PouchDB

> [PouchDB Server](https://github.com/pouchdb/pouchdb-server) is CouchDB-compatible server built on PouchDB and Node.

![pulls](https://img.shields.io/docker/pulls/huiyifyj/pouchdb?logo=docker&style=flat-square&label=pulls)

#### Install

```shell
docker pull huiyifyj/pouchdb
```

#### Usage

```shell
docker run --name pouchdb -p 5984:5984 \
    -v $PWD/data:/data -d huiyifyj/pouchdb
```

Run and wait for it to initialize completely, and visit http://localhost:5984/_utils.
